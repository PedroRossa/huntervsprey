#ifndef FSM_H
#define FSM_H

#include "World.h"

class FSM
{
private:

	World& world;
	World noWorld;

public:
	FSM();
	FSM(World& world);
	~FSM();
};

#endif