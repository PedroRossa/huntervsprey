#include "Character.h"

Character::Character() : worldMap(noMap)
{
}

Character::Character(Map& worldMap) : worldMap(worldMap)
{
}

Character::~Character()
{
}

sf::Vector2u Character::GetPosition() { return this->position; }

void Character::SetPosition(sf::Vector2u position) { this->position = position; }