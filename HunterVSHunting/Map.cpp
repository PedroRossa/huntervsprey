#include "Map.h"

Map::Map()
{
	this->width = 30;
	this->height = 30;

	this->drawLines = false;
}

Map::Map(int width, int height, bool drawlines)
{
	this->width = width;
	this->height = height;

	this->drawLines = drawlines;
}

Map::~Map()
{
}

#pragma region Getters and Setters


int** Map::GetMatrix() { return this->matrix; }

bool Map::DrawLines() { return this->drawLines; }
void Map::DrawLines(bool drawLines) { this->drawLines = drawLines; }
int Map::GetWidth() { return this->width; }
int Map::GetHeight() { return this->height; }
sf::Vector2u Map::GetTileSize() { return this->tileSize; }

void Map::SetNumberOfTrees(int nofTrees) { this->numberOfTrees = nofTrees; }

#pragma endregion

void Map::SetMatrixValue(sf::Vector2u position, hhType value)
{
	this->matrix[position.x][position.y] = value;
}

hhType Map::GetMatrixValue(sf::Vector2u position)
{
	hhType t = (hhType)this->matrix[position.x][position.y];
	return t;
}


void Map::InitMatrix()
{
	if (!grassTexture.loadFromFile("Sprites/grass.jpg"))
		cout << "error to load the grass texture" << endl;

	this->tileSize.x = grassTexture.getSize().x;
	this->tileSize.y = grassTexture.getSize().y;

	this->grassLayer = new sf::Sprite*[this->height];
	this->objectsLayer = new sf::Sprite*[this->height];

	this->matrix = new int*[this->height];
	this->lines = new sf::RectangleShape*[this->height];

	for (size_t i = 0; i < this->height; i++)
	{
		this->matrix[i] = new int[this->width];

		this->grassLayer[i] = new sf::Sprite[this->width];
		this->objectsLayer[i] = new sf::Sprite[this->width];

		this->lines[i] = new sf::RectangleShape[this->width];

		for (int j = 0; j < this->width; j++)
		{
			this->matrix[i][j] = hhType::nothing;

			this->grassLayer[i][j] = sf::Sprite(this->grassTexture);
			this->grassLayer[i][j].setPosition(sf::Vector2f(j * tileSize.x, i * tileSize.y));

			this->lines[i][j] = sf::RectangleShape(sf::Vector2f(this->tileSize));
			this->lines[i][j].setPosition(sf::Vector2f(j * tileSize.x, i * tileSize.y));
			this->lines[i][j].setFillColor(sf::Color(0, 0, 0, 0));
			this->lines[i][j].setOutlineThickness(1);
			this->lines[i][j].setOutlineColor(sf::Color(255, 0, 0, 125));
		}
	}
	CreateTrees();


	FlashAStar astar(this->matrix, this->width, this->height);
	list<vec2> path = astar.GetFlashPath(vec2(0, 0), vec2(16, 18));

	for (std::list<vec2>::const_iterator it = path.begin(), end = path.end(); it != end; ++it)
	{
		this->grassLayer[(int)(*it).v[1]][(int)(*it).v[0]].setColor(sf::Color(255, 0, 0));
	}
}

void Map::DrawMatrixInConsole()
{
	system("cls");
	for (size_t i = 0; i < this->height; i++)
	{
		for (size_t j = 0; j < this->width; j++)
		{
			cout << this->matrix[j][i] << " ";
		}
		cout << endl;
	}
}


void Map::CreateTree()
{

}

void Map::CreateTrees()
{
	if (!treeTexture.loadFromFile("Sprites/tree_1.png"))
		cout << "error to load the tree_1 texture" << endl;

	vector<sf::Vector2i> trees;

	for (size_t i = 0; i < this->numberOfTrees; i++)
	{
		sf::Vector2i treePos;
		bool validPosition = true;

		do
		{
			treePos.x = rand() % (this->width - 1);
			treePos.y = rand() % (this->height - 1);

			for (size_t j = 0; j < trees.size(); j++)
			{
				if (trees[j] == treePos)
				{
					validPosition = false;
					break;
				}
				validPosition = true;
			}
		} while (!validPosition);

		trees.push_back(treePos);


		sf::Vector2u pos(trees[i].x * tileSize.x, trees[i].y * tileSize.y);

		this->objectsLayer[trees[i].x][trees[i].y] = sf::Sprite(this->treeTexture);
		this->objectsLayer[trees[i].x][trees[i].y].setPosition(sf::Vector2f(pos));

		SetMatrixValue(sf::Vector2u(treePos), hhType::tree);
	}
}

void Map::CreateTrees(int nOfTrees)
{
	if (!treeTexture.loadFromFile("Sprites/tree_1.png"))
		cout << "error to load the tree_1 texture" << endl;

	vector<sf::Vector2u> trees;

	for (size_t i = 0; i < nOfTrees; i++)
	{
		sf::Vector2u treePos;
		bool validPosition = true;

		do
		{
			treePos.x = rand() % (this->width - 1);
			treePos.y = rand() % (this->height - 1);

			for (size_t j = 0; j < trees.size(); j++)
			{
				if (trees[j] == treePos)
				{
					validPosition = false;
					break;
				}
				validPosition = true;
			}
		} while (!validPosition);

		trees.push_back(treePos);
		SetMatrixValue(treePos, hhType::tree);
	}


	for (size_t i = 0; i < trees.size(); i++)
	{
		this->objectsLayer[trees[i].x][trees[i].y] = sf::Sprite(this->treeTexture);
		this->objectsLayer[trees[i].x][trees[i].y].setPosition(sf::Vector2f(trees[i].x * tileSize.x, trees[i].y * tileSize.y));
	}
}

void Map::DrawMatrixInSFML(sf::RenderWindow *window)
{
	for (size_t i = 0; i < this->width; i++)
	{
		for (size_t j = 0; j < this->height; j++)
		{
			window->draw(this->grassLayer[j][i]);

			if (this->drawLines)
				window->draw(this->lines[j][i]);
		}
	}

	//TODO: problem with draw Order (This is the only way to see all the trees
	for (size_t i = 0; i < this->width; i++)
	{
		for (size_t j = 0; j < this->height; j++)
		{
			window->draw(this->objectsLayer[j][i]);
		}
	}

}