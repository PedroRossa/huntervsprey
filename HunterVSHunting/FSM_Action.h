#ifndef FSM_ACTION_H
#define FSM_ACTION_H

class FSM_Action
{
public:
	FSM_Action();
	~FSM_Action();

	virtual void Execute() = 0;
};

#endif