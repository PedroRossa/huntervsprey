#ifndef MAP_H
#define MAP_H

#include "Helper.h"
class Map
{
private:
	int numberOfTrees;

	int width;
	int height;

	sf::Vector2u tileSize;

	bool drawLines;

public:

	sf::Texture grassTexture;
	sf::Texture treeTexture;

	int** matrix;
	sf::Sprite** grassLayer;
	sf::Sprite** objectsLayer;

	sf::RectangleShape** lines;

	Map();
	Map(int width, int height, bool drawLines = false);
	~Map();

#pragma region Getters and Setters

	int** GetMatrix();

	bool DrawLines();
	void DrawLines(bool drawLines);
	int GetWidth();
	int GetHeight();
	sf::Vector2u GetTileSize();

	void SetNumberOfTrees(int nofTrees);

#pragma endregion

	void SetMatrixValue(sf::Vector2u position, hhType value);
	hhType GetMatrixValue(sf::Vector2u position);

	void InitMatrix();
	void DrawMatrixInConsole();

	void CreateTree();
	void CreateTrees();
	void CreateTrees(int numberOfTrees);

	void DrawMatrixInSFML(sf::RenderWindow *window);
};

#endif
