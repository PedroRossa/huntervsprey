#include "World.h"

World::World()
{
	srand(time(NULL));

	this->window = new sf::RenderWindow(sf::VideoMode(650, 650), "Hunter VS Prey");

	this->map = new Map(20,20, true);

	this->map->SetNumberOfTrees((rand() % 80));
	this->map->InitMatrix();

	this->numberOfPreys = (rand() % 5) + 6;

	this->CreatePreys();
	this->CreateHunter();


	myClock.restart();
	startTime = myClock.getElapsedTime();
	endTime = myClock.getElapsedTime();
}

World::World(sf::RenderWindow* window, int numberOfPreys, int numberOfTrees)
{
	this->window = window;
	this->map = new Map();
	this->map->InitMatrix();
	srand(time(NULL));

	this->numberOfPreys = numberOfPreys;
	this->map->SetNumberOfTrees(numberOfTrees);

	this->CreatePreys();
	this->CreateHunter();


	myClock.restart();
	startTime = myClock.getElapsedTime();
	endTime = myClock.getElapsedTime();
}

World::~World()
{
}


sf::Window* World::GetWindow() { return this->window; }
Map* World::GetMap() { return this->map; }
Hunter*  World::GetHunter() { return static_cast<Hunter*>(this->hunter); }
vector<Character*>  World::GetPreys() { return this->preys; }
Prey*  World::GetPrey(int id) { return static_cast<Prey*>(this->preys[id]); }

void  World::AddPrey(Prey* prey) { this->preys.push_back(prey); }
void  World::RemovePrey(int id) { this->preys.erase(this->preys.begin() + id); }


void World::CreatePreys()
{
	sf::Vector2u pos;
	bool validPosition = true;
	
	for (size_t i = 0; i < this->numberOfPreys; i++)
	{
		do
		{
			pos.x = rand() % this->GetMap()->GetWidth();
			pos.y = rand() % this->GetMap()->GetHeight();

			for (size_t j = 0; j < this->preys.size(); j++)
			{
				if (preys[j]->GetPosition() == pos || GetMap()->GetMatrixValue(pos) != hhType::nothing)
				{
					validPosition = false;
					break;
				}

				validPosition = true;
			}
		} while (!validPosition);
		

		Character* prey = new Prey(*(this->map), pos);
		this->preys.push_back(prey);
		this->map->SetMatrixValue(pos, hhType::prey);
	}
}

void World::CreateHunter()
{
	sf::Vector2u pos;
	bool validPosition = true;

	do
	{
		pos.x = rand() % this->GetMap()->GetWidth();
		pos.y = rand() % this->GetMap()->GetHeight();

		for (size_t j = 0; j < this->preys.size(); j++)
		{
			if (preys[j]->GetPosition() == pos || GetMap()->GetMatrixValue(pos) != hhType::nothing)
			{
				validPosition = false;
				break;
			}

			validPosition = true;
		}
	} while (!validPosition);

	this->hunter = new Hunter(*(this->map), pos);

	this->map->SetMatrixValue(pos, hhType::hunter);
}

void World::Update()
{
	if (passedTime >= 0.5f)
	{
		this->map->DrawMatrixInConsole();
		
		//UPDATE STATES
		for (size_t i = 0; i < this->preys.size(); i++)
			this->preys[i]->Move(this->preys);

		myClock.restart();

		startTime = myClock.getElapsedTime();
	}

	endTime = myClock.getElapsedTime();
	passedTime = endTime.asSeconds() - startTime.asSeconds();

	
}

void World::WhileUpdate()
{
	while (window->isOpen())
	{
		sf::Event event;
		while (window->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window->close();
		}

		window->clear();

		this->map->DrawMatrixInSFML(this->window);
		this->hunter->Update(this->window);

		for (size_t i = 0; i < this->preys.size(); i++)
		{
			this->preys[i]->Search(this->hunter);
			this->preys[i]->Update(this->window);
		}

		this->Update();

		window->display();
	}
}