#ifndef FLASHASTAR_H
#define FLASHASTAR_H

#include <iostream>
#include <list>
#include "MathFuncs.h"

using namespace std;

enum HEURISTIC
{
	MANHATTAN,
	EUCLIDIAN
};

struct Node
{
	Node* father;
	vec2 position;
	int pathUntilNow;
	int heuristic;
	int totalPath;

	Node(Node* father, vec2 position, int pathUntilNow, int heuristic)
	{
		this->father = father;
		this->position = position;
		this->pathUntilNow = pathUntilNow;
		this->heuristic = heuristic;
		this->totalPath = pathUntilNow + heuristic;
	}

	bool operator<(const Node &a) const
	{
		return (totalPath < a.totalPath);
	}

	bool operator==(const vec2 &a) const
	{
		return ((position.v[0] == a.v[0]) && (position.v[1] == a.v[1]));
	}
};

class FlashAStar
{
private:

	int* matrix;
	int width;
	int height;

	Node* GetNeighborsToOpenList(Node node, list<Node> &openList, list<Node> &closedList, vec2 end, HEURISTIC heuristic);
	

public:

	FlashAStar();
	FlashAStar(int* matrix, int width, int height);
	FlashAStar(int** matrix, int width, int height);
	~FlashAStar();

	list<vec2> GetFlashPath(vec2 start, vec2 end, HEURISTIC heuristic = HEURISTIC::MANHATTAN);
};

#endif