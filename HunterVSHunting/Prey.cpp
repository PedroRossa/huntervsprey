#include "Prey.h"

Prey::Prey(Map& worldMap, sf::Vector2u position)
{
	if (!this->myTexture.loadFromFile("Sprites/pig.png"))
		cout << "Problem to load pig Texture." << endl;

	this->me = sf::Sprite(this->myTexture);

	this->SetPosition(position);
	this->worldMap = worldMap;
}


Prey::~Prey()
{
}

bool Prey::Search(Character* hunter)
{
	int distance = EuclidianDistance(this->GetPosition(), hunter->GetPosition());
	//cout << distance << endl;
	Direction dir = GetDirectionByAngle(this->GetPosition(), hunter->GetPosition(), true);

	return false;
}

void Prey::Move(vector<Character*> preys)
{
	sf::Vector2u newPos;
	bool validPosition = true;

	do
	{
		newPos.x = rand() % 3 - 1;
		newPos.y = rand() % 3 - 1;

		newPos.x += this->GetPosition().x;
		newPos.y += this->GetPosition().y;

		if ((newPos.x > (worldMap.GetWidth() - 1) || newPos.x < 0) || (newPos.y >(worldMap.GetHeight() - 1) || newPos.y < 0))
			validPosition = false;
		else
		{			
			for (size_t j = 0; j < preys.size(); j++)
			{
				if (preys[j]->GetPosition() == newPos)
				{
					validPosition = false;
					break;
				}
				validPosition = true;
			}

			if (this->worldMap.GetMatrixValue(newPos) != hhType::nothing)
				validPosition = false;
		}
	} while (!validPosition);
	
	worldMap.SetMatrixValue(this->GetPosition(), hhType::nothing);
	worldMap.SetMatrixValue(newPos, hhType::prey);

	this->SetPosition(newPos);
}

void Prey::Move(vector<Character*> preys, Direction dir)
{
	sf::Vector2i newPos = ConvertDirectionToValue(dir);
	bool validPosition = true;

	do
	{
		newPos.x += this->GetPosition().x;
		newPos.y += this->GetPosition().y;

		if ((newPos.x > (worldMap.GetWidth() - 1) || newPos.x < 0) ||
			(newPos.y > (worldMap.GetHeight() - 1) || newPos.y < 0) ||
			this->worldMap.GetMatrixValue(sf::Vector2u(newPos)) != hhType::nothing)
		{
			validPosition = false;
		}
		else
		{
			for (size_t j = 0; j < preys.size(); j++)
			{
				if (preys[j]->GetPosition() == sf::Vector2u(newPos))
				{
					validPosition = false;
					break;
				}
				validPosition = true;
			}
		}
	} while (!validPosition);

	this->SetPosition(sf::Vector2u(newPos));
}

void Prey::Update(sf::RenderWindow *window)
{
	sf::Vector2f newPos((this->GetPosition().x * worldMap.GetTileSize().x),
						(this->GetPosition().y * worldMap.GetTileSize().y));

	this->me.setPosition(newPos);

	window->draw(this->me);
}