#ifndef PREY_H
#define PREY_H

#include "Character.h"

class Prey :
	public Character
{
public:
	Prey(Map& worldMap, sf::Vector2u position = sf::Vector2u(0, 0));
	~Prey();


	bool Search(Character* hunter);
	void Move(vector<Character*> preys);
	void Move(vector<Character*> preys, Direction dir);
	void Update(sf::RenderWindow *window);
};

#endif
