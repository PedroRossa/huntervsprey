#ifndef FSM_STATE_H
#define FSM_STATE_H

#include <iostream>
#include <list>
#include "FSM_Action.h"

using namespace std;

class FSM_State
{
private:

	list<FSM_Action*>* OnEnter;
	list<FSM_Action*>* OnExecute;
	list<FSM_Action*>* OnExit;

public:
	FSM_State();
	~FSM_State();

#pragma region Getters

	list<FSM_Action*>* GetOnEnterActions();
	list<FSM_Action*>* GetOnExecuteActions();
	list<FSM_Action*>* GetOnExitActions();

#pragma endregion

#pragma region Add and Delete Actions

	void Add_OnEnterAction(FSM_Action* action);
	void Add_OnExecuteAction(FSM_Action* action);
	void Add_OnExitrAction(FSM_Action* action);

	void Delete_OnEnterAction(FSM_Action* action);
	void Delete_OnExecuteAction(FSM_Action* action);
	void Delete_OnExitrAction(FSM_Action* action);

#pragma endregion


	void Run_OnEnter();
	void Run_Execute();
	void Run_OnExit();
};

#endif