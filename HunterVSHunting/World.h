#ifndef WORLD_H
#define WORLD_H

#include "Map.h"
#include "Hunter.h"
#include "Prey.h"

#include "FlashAStar.h"

class World
{
private:

	Map* map;
	sf::RenderWindow* window;

	Character* hunter;
	vector<Character*> preys;

	int numberOfPreys;


	sf::Clock myClock;
	sf::Time startTime, endTime;
	float passedTime = 9999;

public:
	World();
	World(sf::RenderWindow* window, int numberOfPreys = 10, int numberOfTrees = 20);
	~World();

	sf::Window* GetWindow();
	Map* GetMap();
	Hunter* GetHunter();
	vector<Character*> GetPreys();
	Prey* GetPrey(int id);

	void AddPrey(Prey* prey);
	void RemovePrey(int id);

	void CreatePreys();
	void CreateHunter();

	void Update();
	void WhileUpdate();
};

#endif

