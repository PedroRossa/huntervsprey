#ifndef HELPER_H
#define HELPER_H

#pragma once

#include <iostream>
#include <stdio.h>  
#include <stdlib.h> 
#include <time.h>

#include <SFML\Graphics.hpp>
#include <SFML\System.hpp>


#include "FlashAStar.h"

using namespace std;

	enum hhType
	{
		nothing = 0,
		hunter = 1,
		prey = 2,
		tree = 3
	};

	enum Direction
	{
		north = 0,
		northEast = 1,
		east = 2,
		southEast = 3,
		south = 4,
		southWest = 5,
		west = 6,
		northWest = 7
	};

	static sf::Vector2i ConvertDirectionToValue(Direction dir)
	{
		switch (dir)
		{
		case Direction::north:
			return sf::Vector2i(0, -1);
			break;
		case Direction::northEast:
			return sf::Vector2i(1, -1);
			break;
		case Direction::east:
			return sf::Vector2i(1, 0);
			break;
		case Direction::southEast:
			return sf::Vector2i(1, 1);
			break;
		case Direction::south:
			return sf::Vector2i(0, 1);
			break;
		case Direction::southWest:
			return sf::Vector2i(-1, 1);
			break;
		case Direction::west:
			return sf::Vector2i(-1, 0);
			break;
		case Direction::northWest:
			return sf::Vector2i(-1, -1);
			break;
		default:
			break;
		}
	}

	static int EuclidianDistance(sf::Vector2u objectA, sf::Vector2u objectB)
	{
		int x = objectA.x - objectB.x;
		int y = objectA.y - objectB.y;

		int dist;

		dist = pow(x, 2) + pow(y, 2);
		dist = sqrt(dist);

		return dist;
	}

	static Direction GetDirectionByAngle(sf::Vector2u objectA, sf::Vector2u objectB, bool inverseDirection)
	{
		float dot = (objectA.x * objectB.x) + (objectA.y*objectB.y);
		float det = (objectA.x * objectB.y) - (objectA.y*objectB.x);
		float angle = atan2(det, dot);
		angle = (angle*-1) * 180 / 3.14159265;

		if (angle < 0)
			angle += 360;

		//cout << "Angle: " << angle << " ME: " << objectA.x << "," << objectA.y <<
		//	" Hunter: " << objectB.x << "," << objectB.y << endl;
	
	
		if (angle > 315 || angle <= 45)
		{
			if (inverseDirection)
				return west;
			else
				return east;
		}
		else if (angle > 45 && angle <= 135)
		{
			if (inverseDirection)
				return south;
			else
				return north;
		}
		else if (angle > 135 && angle <= 225)
		{
			if (inverseDirection)
				return east;
			else
				return west;
		}
		else if (angle > 225 && angle <= 315)
		{
			if (inverseDirection)
				return north;
			else
				return south;
		}
		else
			return east;
	}

#endif