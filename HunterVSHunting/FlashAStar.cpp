#include "FlashAStar.h"


FlashAStar::FlashAStar()
{
}

FlashAStar::FlashAStar(int* matrix, int width, int height)
{
	this->matrix = new int[width*height];
	this->matrix = matrix;

	this->width = width;
	this->height = height;
}

FlashAStar::FlashAStar(int** matrix, int width, int height)
{
	this->matrix = new int[width*height];
	for (size_t i = 0; i < height; i++)
	{
		for (size_t j = 0; j < width; j++)
		{
			this->matrix[j + (width * i)] = matrix[j][i];
		}
	}

	this->width = width;
	this->height = height;
}

FlashAStar::~FlashAStar()
{
}

Node* FlashAStar::GetNeighborsToOpenList(Node node, list<Node> &openList, list<Node> &closedList, vec2 end, HEURISTIC heuristic)
{
	//start auxPos on leftDown corner
	vec2 auxPos;
	auxPos.v[0] = node.position.v[0] - 1;
	auxPos.v[1] = node.position.v[1] - 1;

	//check all 8 neighbors
	for (size_t i = 0; i <= 2; i++)
	{
		for (size_t j = 0; j <= 2; j++)
		{
			if ((auxPos.v[0] == node.position.v[0]) && (auxPos.v[1] == node.position.v[1])) //avoid the middle value
			{
				auxPos.v[1]++;
			}
			else if (this->matrix[(int)(auxPos.v[0] + (this->width * auxPos.v[1]))] == 0) //contabilize just free positions (x + y*w)
			{
				//if the position is greather then zero and less than the max of the matrix
				if ((auxPos.v[0] >= 0 && auxPos.v[0] < this->width) &&
					(auxPos.v[1] >= 0 && auxPos.v[1] < this->height))
				{
					//cout << "AUX:" << auxPos.v[0] << "," << auxPos.v[1] << endl;
					list<Node>::iterator& closed_it = find(closedList.begin(), closedList.end(), auxPos);
					
					//Calculate heuristic (H)
					int h;
					if (heuristic == MANHATTAN)
						h = (int)manhattanDistance(auxPos, end);
					else if (heuristic == EUCLIDIAN)
						h = (int)euclidianDistance(auxPos, end);

					//Calculate path Until Now (G)
					int g = node.pathUntilNow;
					//if the both numbers are pair or both are unpair, so it's a corner (14 of cost)
					if (((int)(auxPos.v[0]) % 2 == 0 && (int)(auxPos.v[1]) % 2 == 0) ||
						((int)(auxPos.v[0]) % 2 != 0 && (int)(auxPos.v[1]) % 2 != 0))
						g += 14;
					else
						g += 10;

					Node* n = new Node(&node, auxPos, g, h);

					//if not find on closed list
					if (closed_it == closedList.end())
					{
						list<Node>::iterator& open_it = find(openList.begin(), openList.end(), n->position);

						if (open_it == openList.end())
						{
							openList.push_back(*n);
							//cout << "added on open List" << endl;
						}
					}
					else
					{
						if (g < (*closed_it).heuristic)
						{
							(*closed_it).father = (n);

							//Calculate path Until Now (G) again
							int g = (*closed_it).pathUntilNow;
							//if the both numbers are pair or both are unpair, so it's a corner (14 of cost)
							if (((int)(auxPos.v[0]) % 2 == 0 && (int)(auxPos.v[1]) % 2 == 0) ||
								((int)(auxPos.v[0]) % 2 != 0 && (int)(auxPos.v[1]) % 2 != 0))
								g += 14;
							else
								g += 10;

							(*closed_it).heuristic = g;
							(*closed_it).totalPath = g + (*closed_it).pathUntilNow;

							//cout << "Father of node changed" << endl;
						}
						else
						{
							//cout << "nothing Happens, feijoada" << endl;
						}
					}

					//cout << "POS:(" << n.position.v[0] << "," << n.position.v[1] << ") H: " << h << " G:" << g << " F:" << (g + h) << endl;
				

					if (auxPos.v[0] == end.v[0] && auxPos.v[1] == end.v[1])
					{
						cout << "FOUND!!!!" << endl;
						return n;
					}
				}

				auxPos.v[1]++;
			}
		}
		auxPos.v[0]++;
		auxPos.v[1] = node.position.v[1] - 1;;
	}

	return NULL;
}

list<vec2> FlashAStar::GetFlashPath(vec2 start, vec2 end, HEURISTIC heuristic)
{
	list<Node> openList;
	list<Node> closedList;

	list<vec2> path;

	//Add First element on list
	if (openList.size() <= 0)
	{
		int h;
		if (heuristic == MANHATTAN)
			h = end.v[0] + end.v[1];
		else if (heuristic == EUCLIDIAN)
			h = (int)euclidianDistance(start, end);

		Node n(nullptr, start, 0, h);
		openList.push_back(n);
	}

	do
	{
		//Order list by TotalPathCost
		openList.sort();

		//Get first node
		Node p = openList.front();
		openList.pop_front();

		closedList.push_back(p);
		
		Node* endNode = GetNeighborsToOpenList(p, openList, closedList, end, heuristic);

		if (endNode != NULL)
		{
			Node* n = endNode;

			while (n->father != NULL)
			{
				std::cout << n->position.v[0] << "," << n->position.v[1] << endl;
				path.push_back(n->position);

				n = n->father;
			}

			path.push_back(end);


			break;
		}

		/*
		Fa�a
		Pega o primeiro elemento p da lista aberta com menor F
		Remove p da lista aberta e coloque em uma lista fechada(j� visitados)
		Para cada nodo livre(ou que n�o esteja na lista fechada) a adjacente � p
		Se n�o est�
			adicione a a lista aberta tendo p como pai de a.
			Calcule F, G e H.
		Se est�
			use G como medida.
			Se o novo G for mais baixo que o atual
				mude o pai do nodo para o a,
				recalcule G e F e
				reordene a lista aberta por F

		Enquanto nodo B n�o foi colocado na lista fechada OU tem nodos na
		lista aberta
		*/
	}while (openList.size() > 0); //while have value on open list
	
	for (std::list<vec2>::const_iterator it = path.begin(), end = path.end(); it != end; ++it)
	{
		std::cout <<  (*it).v[0] << "," << (*it).v[1] << endl;
	}

	return path;
}
