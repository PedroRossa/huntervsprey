#ifndef HUNTER_H
#define HUNTER_H

#include "Character.h"
class Hunter: 
	public Character
{
private:
	sf::CircleShape* radiusShape;

public:
	Hunter(Map& map, sf::Vector2u position = sf::Vector2u(0,0));
	~Hunter();
	

	bool Search(Character* hunter);
	void Move(vector<Character*> preys);
	void Move(vector<Character*> preys, Direction dir);
	void Update(sf::RenderWindow *window);
};

#endif