#ifndef CHARACTER_H
#define CHARACTER_H

#include "Map.h"

class Character
{
protected:

	sf::Vector2u position;

	sf::Texture myTexture;
	sf::Sprite me;

	Map& worldMap;
	Map noMap;

public:
	Character();
	Character(Map& worldMap);
	~Character();

	sf::Vector2u GetPosition();
	void SetPosition(sf::Vector2u position);

	virtual bool Search(Character* character) = 0; //Can be prey or Hunter
	virtual void Move(vector<Character*> preys) = 0;
	virtual void Move(vector<Character*> preys, Direction dir) = 0;
	virtual void Update(sf::RenderWindow *window) = 0;
};

#endif